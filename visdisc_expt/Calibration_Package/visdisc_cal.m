%--------------------------------------
% VISUAL DISCRIMINATION CALIBRATOR
%--------------------------------------
%
% Displays pairs of faces for same/difference judgments.
% Returns T, the discrimination threshold and saves data to
% ~-~-C.mat.
%
% Usage:
% T = visdisc_cal(subjno,sessno,runs,nfaces)
%
% T      - threshold value based on mode of morph level trials.
% subjno - subject number as increasing integers (start from subj 1).
% sessno - sessionn number for a subject.
% runs   - number of cycles for a given list of unique faces; also number
%          of catch trials.
% nfaces - number of unique faces in list (current max = 40).
%
%----------- Program History ----------
% 26/07/2007 joshgoh modified from GratingDemo.m
%--------------------------------------

function T = visdisc_cal(subjno,sessno,runs,nfaces)

if nargin < 3
    fprintf('Subject, session, or number of runs not entered!!!\nUse visdisc_cal(subjno,sessno,runs,nfaces)');
    return
end

% PARAMETER SETUP
%--------------------------------------

% Experimental root directory
gpath = pwd;

% Maximum no. of unique face pairs in calibration list. Currently set
% at 40 (40 face pair sets from the total 320; the rest used in actual
% visdisc experiment). Also, this is the number of trials before
% re-initializing face pairs for catch trials (see below).
if ~exist('nfaces')
    maxnofaces = 40
    nfaces = maxnofaces;
else
    maxnofaces = 40;
end

% Response keys
same = '1!'; diff = '3#'; % Use KbName to match

% Instructions and End text
Instructions = sprintf('Face Discrimination\n\n%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s', ...
    'Pairs of faces will be shown to you.\n', ...
    'Your task is to decide if the second face\n', ...
    'looks exactly the SAME or DIFFERENT\n', ...
    'compared to the first face.\n\n', ...
    'When you see the second face,\n', ...
    'if you think it looks exactly the SAME,\n', ...
    'press the 1 button on the number keypad \n', ...
    'to the right of the keyboard \n', ...
    'with your INDEX finger.\n\n', ...
    'If you think it looks DIFFERENT,\n', ... 
    'even if just a little\n', ...
    'press the 3 button on the number keypad \n', ...
    'to the right of the keyboard \n', ...
    'with your MIDDLE finger.\n\n', ...
    'This is a speeded test so respond\n',...
    'as accurately and as fast as you can.\n\n', ...
    'There are no right or wrong answers.\n\n',...
    'Ask now if you have questions.\n', ...
    'If no questions, let the experimenter know,\n', ...
    'and he or she will begin the experiment.');

Finish = sprintf('Good Job!\n\n%s%s%s%s', ...
    'You have completed the experiment.\n\n', ...
    'This screen will terminate\n', ...
    'in a few seconds.\n\n', ...
    'Thank you!');


% Prevents MATLAB from reprinting the source code when the program runs.
echo off

% For an explanation of the try-catch block, see the section "Error Handling"
% at the end of this document.
try

    % ---------- Window Setup ----------
    % Opens a window.

    % Screen is able to do a lot of configuration and performance checks on
    % open, and will print out a fair amount of detailed information when
    % it does.  These commands supress that checking behavior and just let
    % the demo go straight into action.  See ScreenTest for an example of
    % how to do detailed checking.
    oldVisualDebugLevel = Screen('Preference', 'VisualDebugLevel', 3);
    oldSupressAllWarnings = Screen('Preference', 'SuppressAllWarnings', 1);

    % Find out how many screens and use largest screen number.
    whichScreen = max(Screen('Screens'));

    % Hides the mouse cursor
    HideCursor;

    % Opens a graphics window on the main monitor (screen 0).  If you have
    % multiple monitors connected to your computer, then you can specify
    % a different monitor by supplying a different number in the second
    % argument to OpenWindow, e.g. Screen('OpenWindow', 2).
    window = Screen('OpenWindow', whichScreen);


    % ---------- Color Setup ----------

    % Retrieves color codes for black and white and gray.
    black = BlackIndex(window);  % Retrieves the CLUT color code for black.
    white = WhiteIndex(window);  % Retrieves the CLUT color code for white.
    gray = (black + white) / 2;  % Computes the CLUT color code for gray.


    % ---------- Font Setup ----------

    % Text Fonts
    if IsLinux==0
        Screen('TextFont',window, 'Courier New');
        Screen('TextSize',window, 22);
        Screen('TextStyle', window, 0);
    end;

    % ---------- Instruction Display ----------

    % Colors the entire window gray.
    Screen('FillRect', window, black);

    % Writes Instruction text.
    DrawFormattedText(window, Instructions, 'center', 'center', white);

    % Updates the screen to reflect our changes to the window.
    Screen('Flip', window);

    % Waits for the user to press a key.
    KbWait;

    % ---------- Start Face Pair Display ----------
    k = 1; % trial counter
    
    % First fixation
    DrawFormattedText(window, '+', 'center', 'center', white);
    Screen('Flip', window); % Show fixation
    WaitSecs(2); % Wait 2000ms

    % Cycle with 'runs' number of catch trials where pairs are
    % re-initialized to morph level, ml = 0.
    for j = 1:runs

        % Randomize pair order
        pair = randpairorder(gpath,maxnofaces);

        % Initialize first face pair images
        [f1name f2name I1 I2 m pm] = initpair(gpath,pair,0);

        % Trial sequence.
        for i = 1:nfaces
            % Display face pair
            [respcode resptime] = pdisplay(I1,I2,i,window,white); 
            % Log response and update trial count
            if k<=(nfaces*runs)
                resp(k,:) = {f1name, f2name, m, respcode, resptime*1000}; k=k+1; 
            end
            % Select next pair
            [I1, I2, f2name, m, pm] = chooseI(respcode,same,diff,m,pm,i,pair,gpath,nfaces); 
        end

    end % End face pair presentation

    % ---------- Finish Display ----------
    DrawFormattedText(window, Finish, 'center', 'center', white);

    % Updates the screen to reflect our changes to the window.
    Screen('Flip', window);
    WaitSecs(6);


    % ---------- Log Handling ----------
    % Save data to .mat file
    save(sprintf('%d-%d-C.mat',subjno,sessno),'resp');
    
    % Write data to .csv file
    dfname = sprintf('%d-%d-C.csv',subjno,sessno);
    fid = fopen(dfname,'w');
    for i = 1:size(resp,1)
            fprintf(fid,'%s,%s,%d,%s,%.0f\n',resp{i,:});
    end
    fclose(fid);

    % Compute Threshold
    T = mode(cell2mat(resp(:,3)));


    % ---------- Window Cleanup ----------
    % Closes all windows.
    Screen('CloseAll');
    % Restores the mouse cursor.
    ShowCursor;

    % Restore preferences
    Screen('Preference', 'VisualDebugLevel', oldVisualDebugLevel);
    Screen('Preference', 'SuppressAllWarnings', oldSupressAllWarnings);
catch


    % ---------- Error Handling ----------
    % If there is an error in our code, we will end up here.

    % The try-catch block ensures that Screen will restore the display and return us
    % to the MATLAB prompt even if there is an error in our code.  Without this try-catch
    % block, Screen could still have control of the display when MATLAB throws an error, in
    % which case the user will not see the MATLAB prompt.
    Screen('CloseAll');

    % Restores the mouse cursor.
    ShowCursor;

    % Restore preferences
    Screen('Preference', 'VisualDebugLevel', oldVisualDebugLevel);
    Screen('Preference', 'SuppressAllWarnings', oldSupressAllWarnings);

    % We throw the error again so the user sees the error description.
    psychrethrow(psychlasterror);

end


%-------------------------------------
% SUB-FUNCTIONS
%-------------------------------------

% Randomize pair order
%-------------------------------------
function pair = randpairorder(gpath,maxnofaces)

pname = dir(sprintf('%s/stim/calibration/pair*',gpath)); % Read pair name in directory
p = [1:maxnofaces]; % Pair name index in structure
randp = randperm(maxnofaces); % Random pair order
for pi = 1:maxnofaces
    pair{pi} = pname(p(randp(pi))).name; % Randomized pair order vector
end
%-------------------------------------


% Initialize pair
function [f1name f2name I1 I2 m pm] = initpair(gpath,pair,ml)

f1name = sprintf('%s/stim/calibration/%s/%s_%d.bmp',gpath,pair{1},pair{1},ml);
f2name = f1name;
I1 = imread(f1name);
I2 = I1;
m = ml;
pm = 99-ml;


% Pair display function
%-------------------------------------
function [respcode resptime] = pdisplay(I1,I2,i,window,white)

flag1 = 0; flag2 = 0;

Screen('PutImage', window, I1);
Screen('Flip', window); % Show face 1
WaitSecs(0.8); % Wait 800ms
DrawFormattedText(window, '+', 'center', 'center', white);
Screen('Flip', window); % Show fixation
WaitSecs(0.3); % Wait 300ms
Screen('PutImage', window, I2);
FlipTime = Screen('Flip', window); % Show face 2

DrawFormattedText(window, '+', 'center', 'center', white);

while (GetSecs-FlipTime) <= 4
    [ keyIsDown, timeSecs, keyCode ] = KbCheck;
    if (GetSecs-FlipTime > 0.8) & flag1 == 0
        Screen('Flip', window); % Show fixation after 800ms
        flag1 = 1;
    end

    if keyIsDown & flag2 == 0
        respcode = KbName(keyCode);
        resptime = timeSecs - FlipTime;
        flag2 = 1;
    end
end

if flag2 == 0
    respcode = 'na';
    resptime = 'na';
end
%-------------------------------------


% Choose face function
%-------------------------------------
function [I1 I2 f2name m pm] = chooseI(respcode,same,diff,m,pm,i,pair,gpath,nfaces)

if i==nfaces
    I1 = []; I2 = []; f2name = []; m = []; pm = [];
else
    % Choose I1
    f1name = sprintf('%s/stim/calibration/%s/%s_%d.bmp',gpath,pair{i+1},pair{i+1},0);
    I1 = imread(f1name);

    % Choose I2 - Anchorpoint is morph 0
    ppm = m; % Assign previous morph value
    if (m<98 & strmatch(respcode,same))    % If not morph level 99 previous response is SAME
        m = m + 1 + ceil(abs(pm - m)/2);      % Next I2 will be ~half from current I2 and prev I2 towards 99
        if (m>99)
            m=99;
        end
    elseif (m>1 & strmatch(respcode,diff)) % If previous response is DIFFERENT and morph level is not 0
        m = m - 1 - floor(abs(pm - m)/2);      % Next I2 will be ~half from current I2 and prev I2 towards 0
        if (m<0)
            m=0;
        end
    elseif (m==0) % If morph level is 0 and no response
        m = m;  % Next I2 will be the same as current I2
        ppm = 99;
    else
        m = m;
    end
    pm = ppm;

    f2name = sprintf('%s/stim/calibration/%s/%s_%d.bmp',gpath,pair{i+1},pair{i+1},m);
    I2 = imread(f2name);
end
%-------------------------------------
