# Face Discrimination Experiment

- Repo for Goh, J. O., Suzuki, A., & Park, D. C. (2010). Reduced neural selectivity increases fMRI adaptation with age during face discrimination. Neuroimage, 51(1), 336–344. https://doi.org/10.1016/j.neuroimage.2010.01.107
- This repo was uploaded on 1 Apr 2022 for public sharing purposes and will not be actively maintained.
- Issues might be addressed to joshuagoh@ntu.edu.tw.

## Contents
1. Experimental protocol checklists and delivery instructions.
2. List of neuropsychological assessments. Actual tests will have to be purchase under copyright laws.
3. Stimuli generation tools.
    - Scripts for applying vignette frame.
    - Sqirlz Morph software used for face morphing.
4. E-Prime stimuli presentation files.
5. Behavioral analysis scripts.
6. Brain voyager brain analysis scripts.
7. Matlab brain analysis scripts.
8. Conference abstracts and posters.
9. Summary presentation slides.

## Stimuli access procedure
- Because of file storage size and privacy concerns, for the actual face stimuli please email your request to Joshua Goh (joshuagoh@ntu.edu.tw) stating your contact details and the purpose for which you plan to use the stimuli. 
- If your usage is within the permit of the ethical regulations pertaining to these stimuli use, access will be granted to a compressed file containing the stimuli files. 
- The uncompressed contents can be rsync-ed with the visdisc_expt directory in this repo to maintain relative paths implemented in this experiment.
