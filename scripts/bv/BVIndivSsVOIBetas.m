% ------------------------------------------------------------
% BV Matlab Toolbox function BVIndivSsVOIBetas.m
% ------------------------------------------------------------
%
% Usage: B = BVIndivSsVOIBetas(voifname,bbox)
%
% Compute betas from vtc list based on voxels obtained from individual
% subject vois. Exports results to B_voiname.csv and outputs and saves same to B.
% voifname - voi variable obtained from .voi read by BVQXfile. VOIs for
%            this function must be named the subject code to obtain vtc files.
%            VOIs must be sorted in the same order at vtcs in glmfname.
% bbox     - structure containing the fields: .BBox, .DimXYZ, .FCube, .ResXYZ.
%            Check help bvcoordconv for details.
%
% Note: Every subject directory must have FIR_DesignMatrix.txt that
% contains the design matrix generated from BV compute GLM.
% ------------------------------------------------------------

function B = BVIndivSsVOIBetas(voifname,bbox)

voi = BVQXfile(voifname);

% Get parameters
NrOfSubjects    = 40;
NrOfStudies     = 320;
NrOfRuns        = NrOfStudies/NrOfSubjects;
basedir         = '/net/data9/visdisc/bv/';

k = 1; % VTC counter

% Loop Subjects
for s = 1:NrOfSubjects

    % Print subject progress
    s,

    % Read VTCs
    for r = 1:NrOfRuns
        vtclist{r} = voi.VTCList{k};
        k=k+1;
    end
    vtc = vtc_concat('temp.vtc',vtclist);
    vtc = BVQXfile('temp.vtc');

    % Read design matrix for subject
    Xfilename = sprintf('%s%s/FIR_DesignMatrix.txt',basedir,voi.VTCList{k-1}(23:29));
    X = load(Xfilename);

    % Loop VOIs
    for v = 1:length(voi.VOI)

        % Convert VOI TAL coord to BV internal volume indices
        C = bvcoordconv(voi.VOI(v).Voxels,'tal2bvx',bbox);

        % Mask with VOI and average timecourse
        Y = mean(vtc.VTCData(:,C),2);

        % Compute GLM and extract betas
        b = inv(X'*X)*X'*Y;
        B.VOI(v).Betas(s,:) = b';
        B.VOI(v).Name = voi.VOI(v).Name;

        % Clean up VOI Loop
        clear C Y b;

    end % End VOI Loop

    % Clean up Subject Loop
    olist{1} = vtc;
    clearbvqxobjects(olist);
    clear X Xfilename;
    system('rm temp.vtc');

end % End Subject loop

% Save B and Write to .csv
for v = 1:length(voi.VOI)
    csvwrite(sprintf('B_%s.csv',voi.VOI(v).Name),B.VOI(v).Betas);
end

save B B
