% ------------------------------------------------------------
% BV Matlab Toolbox function BVGLMCorr.m
% ------------------------------------------------------------
%
% Usage: BVGLMCorr(glmfname,con,bbox)
%
% Compute correlations for each vector in C for each voxel in glm, for a
% given computed variable. Exports results to fname.vmp.
% glmfname - filename of glm. GLMData will first be discarded to save space.
% con      - structure containing fields for each correlation,
%            C       - vertical vectors for correlation. C values must be in the
%                      same order as the subjects listed in glm.
%            fname   - structure with field "name" containing strings of
%                      output .vmp filenames.
%            ssorder - vector of sorted subject positions for subject
%                      selection.
%            sorder   - vector of subject no. positions for sorting.
% bbox     - structure containing the fields: .BBox, .DimXYZ, .FCube, .ResXYZ.
%            Check help bvcoordconv for details.
%
% Note: This script is currently set to run correlations on two types of
% variables. See Step 1 comments in script.
% ------------------------------------------------------------

function BVGLMCorr(glmfname,con,bbox)

% Read glmfname
glm = BVQXfile(glmfname);

% Get parameters
NrOfSubjects        = glm.NrOfSubjects;
NrOfSubjPredictors  = glm.NrOfSubjectPredictors;
NrOfStudies         = glm.NrOfStudies;
xDim                = (glm.XEnd-glm.XStart)/glm.Resolution;
yDim                = (glm.YEnd-glm.YStart)/glm.Resolution;
zDim                = (glm.ZEnd-glm.ZStart)/glm.Resolution;

% Step 1:
% Compute variable in voxels for correlation with C.
% This is currently set for the VDR project as follows:
% For a set task t/d,
% For each subject,
% Across all voxels,
% 40% cond adaptation = 100% cond - 40% cond (if correlating m2 adpt)
% 40% effort = 2*(40% cond) - (100% cond + 0% cond) (if correlating effort)
% 0% cond adaptation = 100% cond - 0% cond (if correlating m1 adpt)
%
% Step 2:
% For each voxel,
% Obtain all subject estimates of variable,
% Correlate the result with C.

% Start subject loop
Bm2adpt = zeros(xDim,yDim,zDim,NrOfSubjects);
Bm2eff  = zeros(xDim,yDim,zDim,NrOfSubjects);
Bm1adpt = zeros(xDim,yDim,zDim,NrOfSubjects);
sorder  = con(1).sorder; % Subject ordering vector
for s = 1:NrOfSubjects

    % First, clear some space by retaining only the needed predictor betas and
    % deleting the rest of the info in glm.GLMData

    % Uncomment this set for d-task.
    % glm.GLMData.Subject(s).BetaMaps(:,:,:,1) = glm.GLMData.Subject(s).BetaMaps(:,:,:,41); % Set 0% cond, peak tp 5.
    % glm.GLMData.Subject(s).BetaMaps(:,:,:,2) = glm.GLMData.Subject(s).BetaMaps(:,:,:,50); % Set 40% cond, peak tp 5.
    % glm.GLMData.Subject(s).BetaMaps(:,:,:,3) = glm.GLMData.Subject(s).BetaMaps(:,:,:,59); % Set 100% cond, peak tp 5.

    % Uncomment this set for t-task.
    glm.GLMData.Subject(s).BetaMaps(:,:,:,1) = glm.GLMData.Subject(s).BetaMaps(:,:,:,5); % Set 0% cond, peak tp 5.
    glm.GLMData.Subject(s).BetaMaps(:,:,:,2) = glm.GLMData.Subject(s).BetaMaps(:,:,:,14); % Set 40% cond, peak tp 5.
    glm.GLMData.Subject(s).BetaMaps(:,:,:,3) = glm.GLMData.Subject(s).BetaMaps(:,:,:,23); % Set 100% cond, peak tp 5.

    glm.GLMData.Subject(s).BetaMaps(:,:,:,4:end) = []; % Clear everything else.

    % Compute variables for correlation.
    Bm2adpt(:,:,:,sorder(s)) = glm.GLMData.Subject(s).BetaMaps(:,:,:,3) - glm.GLMData.Subject(s).BetaMaps(:,:,:,2);
    Bm2eff(:,:,:,sorder(s)) = 2*glm.GLMData.Subject(s).BetaMaps(:,:,:,2) - (glm.GLMData.Subject(s).BetaMaps(:,:,:,1) + glm.GLMData.Subject(s).BetaMaps(:,:,:,3));
    Bm1adpt(:,:,:,sorder(s)) = glm.GLMData.Subject(s).BetaMaps(:,:,:,3) - glm.GLMData.Subject(s).BetaMaps(:,:,:,1);
end

% Initialize VMP info
vmp   = newnatresvmp([bbox.BBox, 3]);
NMaps = 1;

% Start correlation map loops
for cc = 1:length(con)

    % Set parameters
    C       = con(cc).C; % Correlation vector
    ssorder = con(cc).ssorder; % Subject selection vector
    fname   = con(cc).fname; % Output .vmp filenames

    % Sorts to equate Bs and Cs subject order
    C = [C sorder];
    C = sortrows(C,2);

    % Start voxel loops
    Rm2adpt = zeros(xDim,yDim,zDim);
    Rm2eff  = zeros(xDim,yDim,zDim);
    Rm1adpt = zeros(xDim,yDim,zDim);
    for ix = 1:xDim
        for iy = 1:yDim
            for iz = 1:zDim
                % Sort subjects if required
                rm2adpt = corrcoef(squeeze(Bm2adpt(ix,iy,iz,ssorder)),C(ssorder,1));
                rm2eff  = corrcoef(squeeze(Bm2eff(ix,iy,iz,ssorder)),C(ssorder,1));
                rm1adpt = corrcoef(squeeze(Bm1adpt(ix,iy,iz,ssorder)),C(ssorder,1));
                if ~isnan(rm2adpt(2,1))
                    Rm2adpt(ix,iy,iz) = rm2adpt(2,1);
                end
                if ~isnan(rm2eff(2,1))
                    Rm2eff(ix,iy,iz) = rm2eff(2,1);
                end
                if ~isnan(rm1adpt(2,1))
                    Rm1adpt(ix,iy,iz) = rm1adpt(2,1);
                end
            end
        end
    end

    % Associate with corresponding correlation variables and write VMPs.
    for v = 1:length(fname)
        if v==1
            vmp.Map(NMaps).VMPData = Rm2adpt;
        elseif v==2
            vmp.Map(NMaps).VMPData = Rm2eff;
        elseif v==3
            vmp.Map(NMaps).VMPData = Rm1adpt;
        end
        vmp.Map(NMaps).Type                     = 2;
        vmp.Map(NMaps).ClusterSize              = 50;
        vmp.Map(NMaps).EnableClusterCheck       = 0;
        vmp.Map(NMaps).LowerThreshold           = 0.31;
        vmp.Map(NMaps).UpperThreshold           = 1;
        vmp.Map(NMaps).UseValuesAboveThresh     = 1;
        vmp.Map(NMaps).DF1                      = length(ssorder)-2; % Account for subject sort
        vmp.Map(NMaps).DF2                      = 0;
        vmp.Map(NMaps).ShowPositiveNegativeFlag = 3;
        vmp.Map(NMaps).BonferroniValue          = 69319;
        vmp.Map(NMaps).RGBLowerThreshPos        = [100 0 0];
        vmp.Map(NMaps).RGBUpperThreshPos        = [255 0 0];
        vmp.Map(NMaps).RGBLowerThreshNeg        = [0 0 100];
        vmp.Map(NMaps).RGBUpperThreshNeg        = [0 0 255];
        vmp.Map(NMaps).UseRGBColor              = 0;
        vmp.Map(NMaps).TransColorFactor         = 1;
        vmp.Map(NMaps).Name                     = sprintf('<%s>',fname(v).name);
        % vmp.NrOfMaps                            = NMaps;
        % NMaps                                   = NMaps + 1;   
        
        % Write VMP
        vmp.SaveAs(sprintf('%s.vmp',fname(v).name));
        
    end
end % End correlation map loop

