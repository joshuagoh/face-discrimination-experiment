
//
// Example script for preprocessing a FMR project (single run)
// Feel free to adapt this script to your needs
//
//  Created by Rainer Goebel, last modified November 03 2005
//

function Preprocess_FMR()
{
    var ret = BrainVoyagerQX.TimeOutMessageBox( "This script function will run standard FMR preprocessing steps.\n\nYou can cancel this script by pressing the 'ESCAPE' key.", 8);
    if( !ret ) return;
 
    //
    // Create a new FMR or open a previously created one.  Here we open the "CG_OBJECTS_SCRIPT.fmr" file
    //
    var docFMR = BrainVoyagerQX.OpenDocument(ObjectsRawDataPath  + "CG_OBJECTS_SCRIPT.fmr");

    //
    // Set spatial and temporal parameters relevant for preprocessing
    // You can skip this, if you have checked that these values are set when reading the data
    // To check whether these values have been set already (i.e. from header), use the "VoxelResolutionVerified" and "TimeResolutionVerified" properties
    //
    if( !docFMR.TimeResolutionVerified )
    {
	docFMR.TR = 2000;
	docFMR.InterSliceTime = 80;
	docFMR.TimeResolutionVerified = true;
    }
    if( !docFMR.VoxelResolutionVerified )
    {
	docFMR.PixelSizeOfSliceDimX = 3.5;
	docFMR.PixelSizeOfSliceDimY = 3.5;
	docFMR.SliceThickness = 3;
	docFMR.GapThickness = 0.99;
	docFMR.VoxelResolutionVerified = true;
    }
 
    // We also link the PRT file, if available (if no path is specified, the program looks in folder of dcoument)
    docFMR.LinkStimulationProtocol( "CG_OBJECTS.prt" );
 
    // We save the new settings into the FMR file
    docFMR.Save();
 
    //
    // Preprocessing step 1: Slice time correction
    //
    ret = BrainVoyagerQX.TimeOutMessageBox("Preprocessing step 1: Slice time correction.\n\nTo skip this step, press the 'ESCAPE' key.", 5);
    if(ret)
    {
	// docFMR.SliceTimeCorrection( 1 );        // 0  -> Ascending,  1  -> Asc-Interleaved,  2  -> Asc-Int2,   10 -> Descending,  11 -> Desc-Int,  12 -> Desc-Int2
	docFMR.CorrectSliceTiming( 1, 0 ); // New v1.5
  // First param: Scan order 0  -> Ascending,  1  -> Asc-Interleaved,  2  -> Asc-Int2,   10 -> Descending,  11 -> Desc-Int,  12 -> Desc-Int2
  // Second param: Interpolation method: 0 -> trilinear, 1 -> sinc

  ResultFileName = docFMR.FileNameOfPreprocessdFMR;
  docFMR.Close();           // docFMR.Remove();  // close or remove input FMR
  docFMR = BrainVoyagerQX.OpenDocument( ResultFileName );
 }
 //
 // Preprocessing step 2: 3D motion correction
 //
 ret = BrainVoyagerQX.TimeOutMessageBox("Preprocessing step 2: 3D motion correction.\n\nTo skip this step, press the 'ESCAPE' key.", 5);
 if(ret)
 {
  docFMR.MotionCorrection3D();
  ResultFileName = docFMR.FileNameOfPreprocessdFMR;  // the current doc (input FMR) knows the name of the automatically saved output FMR
  docFMR.Close();            // close input FMR
  docFMR = BrainVoyagerQX.OpenDocument( ResultFileName ); // Open motion corrected file (output FMR) and assign to our doc var
 }
 //
 // Preprocessing step 3: Spatial Gaussian Smoothing    (not recommended for individual analysis with a 64x64 matrix)
 //
 ret = BrainVoyagerQX.TimeOutMessageBox("Preprocessing step 3: Spatial gaussian smoothing.\n\nTo skip this step, press the 'ESCAPE' key.", 5);
 if(ret)
 {
  docFMR.SpatialGaussianSmoothing( 4, "mm" );    // FWHM value and unit

  ResultFileName = docFMR.FileNameOfPreprocessdFMR;
  docFMR.Close();           // docFMR.Remove();  // close or remove input FMR
  docFMR = BrainVoyagerQX.OpenDocument( ResultFileName );
 }
 //
 // Preprocessing step 4: Temporal High Pass Filter, includes Linear Trend Removal
 //
 ret = BrainVoyagerQX.TimeOutMessageBox("Preprocessing step 4: Temporal high-pass filter.\n\nTo skip this step, press the 'ESCAPE' key.", 5);
 if(ret)
 {
  docFMR.TemporalHighPassFilter( 3, "cycles" );

  ResultFileName = docFMR.FileNameOfPreprocessdFMR;
  docFMR.Close();           // docFMR.Remove();  // close or remove input FMR
  docFMR = BrainVoyagerQX.OpenDocument( ResultFileName );
 }
 //
 // Preprocessing step 5: Temporal Gaussian Smoothing  (not recommended for event-related data)
 //
 ret = BrainVoyagerQX.TimeOutMessageBox("Preprocessing step 5: Temporal gaussian smoothing.\n\nTo skip this step, press the 'ESCAPE' key.", 5);
 if(ret)
 {
  docFMR.TemporalGaussianSmoothing( 10, "s" );

  ResultFileName = docFMR.FileNameOfPreprocessdFMR;
  docFMR.Close();           // docFMR.Remove();  // close or remove input FMR
  docFMR = BrainVoyagerQX.OpenDocument( ResultFileName );
 }
 
 // docFMR.Close() // you may want to close the final document, i..e to preprocess another run
}

function MotionCorrection()
{
 var docFMR = BrainVoyagerQX.ActiveDocument();
 
 docFMR.CorrectMotion(1); // new param: target volume, with "1" this is the same as: docFMR.MotionCorrection3D();
 
 // for intra-session motion correction use this command (with approprate file name):
    // docFMR.CorrectMotionTargetVolumeInOtherRun("run1.fmr", 1);
 
 ResultFileName = docFMR.FileNameOfPreprocessdFMR;
 docFMR.Close();
 docFMR = BrainVoyagerQX.OpenDocument( ResultFileName );
}
