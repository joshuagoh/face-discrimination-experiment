% ------------------------------------------------------------
% BV Matlab Toolbox function BVVMPCorr.m
% ------------------------------------------------------------
%
% Usage: BVVMPCorr(vmpfname,con,bbox,sorder)
%
% Compute correlations for each voxel in vmp with variables in con. Exports results to fname.vmp.
% vmpfname - filename of glm. GLMData will first be discarded to save space.
% con      - structure containing fields,
%            C       - vertical vectors for correlation. C values must be in the
%                      same order as the subjects listed in glm.
%            fname   - string of output .vmp filenames.
%            ssorder - vector of sorted subject positions for subject
%                      selection.
% bbox     - structure containing the fields: .BBox, .DimXYZ, .FCube, .ResXYZ.
%            Check help bvcoordconv for details.
% sorder   - vector of subject no. positions for sorting.
%
% NOTE: This script is still a work in progres!!! - Josh 12 June 2008
% ------------------------------------------------------------

function BVVMPCorr(vmpfname,con,bbox,sorder)

% Read vmpfname
vmp = BVQXfile(vmpfname);

% Get parameters
NrOfSubjects        = glm.NrOfSubjects;
NrOfSubjPredictors  = glm.NrOfSubjectPredictors;
NrOfStudies         = glm.NrOfStudies;
xDim                = (glm.XEnd-glm.XStart)/glm.Resolution;
yDim                = (glm.YEnd-glm.YStart)/glm.Resolution;
zDim                = (glm.ZEnd-glm.ZStart)/glm.Resolution;

% Start subject loop
B = zeros(xDim,yDim,zDim,NrOfSubjects);

% Sort subjects
for s = 1:NrOfSubjects
    B(:,:,:,sorder(s)) = vmp.Subject(s).data(:,:,:);
end

% Start correlation map loops
for cc = 1:length(con)

    % Set parameters
    C       = con(cc).C; % Correlation vector
    ssorder = con(cc).ssorder; % Subject selection vector
    fname   = con(cc).fname; % Output .vmp filenames

    % Sorts to equate Bs and Cs subject order
    C = [C sorder];
    C = sortrows(C,2);

    % Start voxel loops
    R = zeros(xDim,yDim,zDim);
    for ix = 1:xDim
        for iy = 1:yDim
            for iz = 1:zDim
                % Sort subjects if required
                r = corrcoef(squeeze(B(ix,iy,iz,ssorder)),C(ssorder,1));
                if ~isnan(r(2,1))
                    R(ix,iy,iz) = r(2,1);
                end
            end
        end
    end

    % Write VMPs
    % Set VMP info
    vmp                              = newnatresvmp([bbox.BBox, 3]);
    vmp.Map.Type                     = 2;
    vmp.Map.ClusterSize              = 50;
    vmp.Map.EnableClusterCheck       = 0;
    vmp.Map.LowerThreshold           = 0.31;
    vmp.Map.UpperThreshold           = 1;
    vmp.Map.UseValuesAboveThresh     = 1;
    vmp.Map.DF1                      = length(ssorder)-2; % Account for subject sort
    vmp.Map.DF2                      = 0;
    vmp.Map.ShowPositiveNegativeFlag = 3;
    vmp.Map.BonferroniValue          = 69319;
    vmp.Map.RGBLowerThreshPos        = [100 0 0];
    vmp.Map.RGBUpperThreshPos        = [255 0 0];
    vmp.Map.RGBLowerThreshNeg        = [0 0 100];
    vmp.Map.RGBUpperThreshNeg        = [0 0 255];
    vmp.Map.UseRGBColor              = 0;
    vmp.Map.TransColorFactor         = 1;

    % Associate with corresponding correlation variables and write VMPs.
    vmp.Map.VMPData = R;
    vmp.Map.Name = sprintf('<%s>',con(cc).fname);
    vmp.SaveAs(sprintf('%s.vmp',con(cc).fname));
end % End correlation map loop