
function CreateVTCFile()
{
	var bvqx = Application.BrainVoyagerQX;
 
	bvqx.SetCurrentDirectory(ObjectsRawDataPath);
 
	var docVMR = bvqx.OpenDocument("CG_3DT1MPR_INH.vmr");
 
	// for this test, only transform first volume (useful to check VTC - VMR-TAL alignment quality!)
	docVMR.CreateVTC("CG_OBJECTS_firstvol.fmr", "CG_OBJECTS_3DMC_SCSAI_LTR_THP3c-TO-CG_3DT1MPR_INH_IA.trf", "CG_OBJECTS_3DMC_SCSAI_LTR_THP3c-TO-CG_3DT1MPR_INH_FA.trf", "CG_3DT1MPR_INH_ACPC.trf", "CG_3DT1MPR_INH_ACPC.tal", "CG_OBJECTS_FirstVol_TAL_FromScript.vtc");
	docVMR.Close();
 
	// now load the TAL VMR and link the newly created VTC file
	var docTalVMR = bvqx.OpenDocument("CG_3DT1MPR_INH_TAL.vmr");
	docTalVMR.LinkVTC("CG_OBJECTS_FirstVol_TAL_FromScript.vtc");
}

function CreateVTCFileInVMRSpace()
{
	var bvqx = Application.BrainVoyagerQX;
 
	bvqx.SetCurrentDirectory(ObjectsRawDataPath);
  
	var docVMR = bvqx.OpenDocument("CG_3DT1MPR_INH.vmr");
 
	// for this test, only transform first volume (useful to check VTC - VMR-TAL alignment quality!)
	docVMR.CreateVTCInVMRSpace("CG_OBJECTS_firstvol.fmr", "CG_OBJECTS_3DMC_SCSAI_LTR_THP3c-TO-CG_3DT1MPR_INH_IA.trf", "CG_OBJECTS_3DMC_SCSAI_LTR_THP3c-TO-CG_3DT1MPR_INH_FA.trf", "CG_OBJECTS_FirstVol_VMR_Res1_FromScript.vtc", 1, 1, 100);
 
	// now link the newly created VTC file (we need not to load a VMR because the present VMR fits the VTC space since FMR is aligned to this space)
	docVMR.LinkVTC("CG_OBJECTS_FirstVol_VMR_Res1_FromScript.vtc");
}

function MakeProtocol()
{
	var bvqx = Application.BrainVoyagerQX;
 
	doc = bvqx.ActiveDocument(); 

	doc.ClearStimulationProtocol();
 
	doc.StimulationProtocolExperimentName = "Objects Experiment";
 
	doc.StimulationProtocolResolution = 1;

	doc.AddCondition("Fixation");
	doc.AddCondition("Objects in LVF");
	doc.AddCondition("Objects in RVF");
	doc.AddCondition("Objects in BVF");
 
	for(interval = 0; interval < 10; interval++)
	{
		intervalstart = interval*27 - 1;
		intervalend   = intervalstart + 8;

		// first interval is shorter:
		if(interval == 0)
		{
			intervalstart = 1;
			intervalend = 7;
		}
		doc.AddInterval("Fixation", intervalstart, intervalend);
	}

	for(interval = 0; interval < 3; interval++)
	{
		intervalstart = 35+interval*81;
		intervalend   = intervalstart + 17;

		doc.AddInterval("Objects in LVF", intervalstart, intervalend);
	}

	for(interval = 0; interval < 3; interval++)
	{
		intervalstart = 8+interval*81;
		intervalend   = intervalstart + 17;

		doc.AddInterval("Objects in RVF", intervalstart, intervalend);
	}

	for(interval = 0; interval < 3; interval++)
	{
		intervalstart = 62+interval*81;
		intervalend   = intervalstart + 17;
   
		doc.AddInterval("Objects in BVF", intervalstart, intervalend);
	}

	doc.SetConditionColor("Fixation", 100, 100, 100);
	doc.SetConditionColor("Objects in LVF", 255, 0, 0);
	doc.SetConditionColor("Objects in RVF", 0, 255, 0);
	doc.SetConditionColor("Objects in BVF", 0, 0, 255);

	doc.StimulationProtocolBackgroundColorR = 0;
	doc.StimulationProtocolBackgroundColorG = 0;
	doc.StimulationProtocolBackgroundColorB = 0;

	doc.StimulationProtocolTimeCourseColorR = 255;
	doc.StimulationProtocolTimeCourseColorG = 255;
	doc.StimulationProtocolTimeCourseColorB = 255;
	doc.StimulationProtocolTimeCourseThickness = 4;

	doc.SaveStimulationProtocol("CG_OBJECTS_FROMSCRIPT.prt");

	// MessageBox.information(String("NrOfConditions: %1".arg(doc.NrOfStimulationProtocolConditions)));

	doc.Save(); // to save link to protocol permanently
}
