% ------------------------------------------------------------
% BV Matlab Toolbox function BVVOIIndivSsGLMBetas.m
% ------------------------------------------------------------
% 
% Usage: BVVOIIndivSsGLMBetas(glmfname,voi,bbox)
% 
% Compute betas from vtclist based on glm masked with voxels obtained from
% mask voi. Exports results to B_voiname.csv for each voi in voi.
% glmfname - filename of .glm file to obtain glm variable using BVQXfile.
%            GLMData will first be discarded to save space.
% voi      - voi variable obtained from .voi read by BVQXfile.
% bbox     - structure containing the fields: .BBox, .DimXYZ, .FCube, .ResXYZ.
%            Check help bvcoordconv for details.
% ------------------------------------------------------------

function BVVOIIndivSsGLMBetas(glmfname,voi,bbox)

glm = BVQXfile(glmfname);

% Get parameters
NrOfSubjects    = glm.NrOfSubjects;
NrOfPredictors  = glm.NrOfPredictors;
NrOfStudies     = glm.NrOfStudies;
Study           = glm.Study;

% Free space
glm.GLMData=[];

% Loop VOIs
for v = 1:length(voi.VOI)
    
    % Initialize data matrix B
    B = zeros(NrOfSubjects,((NrOfPredictors/NrOfSubjects)+(NrOfStudies/NrOfSubjects)-1));
    
    % Convert VOI TAL coord to BV internal volume indices
    C = bvcoordconv(voi.VOI(v).Voxels,'tal2bvx',bbox);
    
    k = 1; % Study counter
    
    % Loop subjects in GLM
    for s = 1:NrOfSubjects
        
        [v s]
        
        % Read VTCs
        for r = 1:NrOfStudies/NrOfSubjects
            vtclist{r} = Study(k).NameOfAnalyzedFile;
            k = k+1;
        end
        vtc = vtc_concat('temp.vtc',vtclist);
        vtc = BVQXfile('temp.vtc');
        
        % Mask with VOI and average timecourse
        Y = mean(vtc.VTCData(:,C),2);
        
        % Read design matrix
        Xfilename = sprintf('%sFIR_DesignMatrix.txt',Study(k-1).NameOfAnalyzedFile(1:30));
        X = load(Xfilename);
                
        % Compute GLM and extract betas
        b = inv(X'*X)*X'*Y;
        B(s,:) = b';
        
        % Clean up
        olist{1} = vtc;
        clearbvqxobjects(olist);
        clear Y b X Xfilename
        system('rm temp.vtc');
        
    end % End subject loop
    
    csvwrite(sprintf('B_%s.csv',voi.VOI(v).Name),B);
    clear B C;
    
end % End voi loop