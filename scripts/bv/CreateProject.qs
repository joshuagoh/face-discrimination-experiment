
function Create_FMR_Project()
{
    var bvqx = Application.BrainVoyagerQX;
    
    var docFMR = bvqx.CreateProjectMosaicFMR("DICOM", ObjectsRawDataPath + "BetSog_20040312_Goebel_C2 -0003-0001-0001.dcm", 252, 2, true, 25, "untitled-", false, 320, 320, 2, ObjectsRawDataPath, 1, 64, 64 );
    
    if( !CheckDoc( docFMR ) ) // in case of error, stop execution of script function
	return;
    
    docFMR.SaveAs( "CG_OBJECTS_SCRIPT.fmr" );
}

function Create_VMR_Project() 
{
    var bvqx = Application.BrainVoyagerQX;
    
    var docVMR = bvqx.CreateProjectVMR( "DICOM", ObjectsRawDataPath + "BetSog_20040312_Goebel_C2 -0002-0001-0001.dcm", 192, false, 256, 256, 2 );
    
    if( !CheckDoc( docVMR ) ) // in case of error, stop execution of script function
	return;
    
    docVMR.SaveAs( "CG_3DT1MPR_SCRIPT.vmr" );
}

function Create_AMR_Project()
{
    var bvqx = Application.BrainVoyagerQX;
    
    var docAMR = bvqx.CreateProjectAMR( "DICOM", ObjectsRawDataPath + "BetSog_20040312_Goebel_C2 -0001-0001-0001.dcm", 3, false, 256, 256, 2 );
    
    docAMR.SaveAs("CG_SLICELOCALIZER_SCRIPT.amr");
}
