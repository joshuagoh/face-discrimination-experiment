function data = vd_get_meanrt(S)

% S - string of subject directories

data = [];

for s = 1:length(S)
Tfname = sprintf('/net/data9/visdisc/behav_output/visdisc/actual/%s/T.mat',S(s,:));
load(Tfname);
data = vertcat(data,[str2num(S(s,:)) mean(T(2).cond(1).rt) mean(T(2).cond(2).rt) mean(T(2).cond(3).rt)]);
end
