function Y = get_voxel_data(confiles,mnicoord)

% Converts MNI coords to indices and extracts img values.
%
% Usage: Y = get_voxel_data(confiles,voxcoord)
%
% confile: contrast file(s) to use
% mnicoord: seed voxel coordinate

% Read confiles
V = spm_vol(confile);
params = spm_imatrix(V(1).V.mat);

% Convert MNI voxel coord (in mm) to volume index
voxcoord(:,1:3) = (mnicoord - ones(size(RY.Yorig,4),1)*params(1:3))/diag(params(7:9));

