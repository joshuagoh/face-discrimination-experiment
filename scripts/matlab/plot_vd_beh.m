function [T cM] = plot_vd_beh(subjno,saveflag)

% Part 1
% Reads and extracts data from behav files for visdisc runs
% Outputs data into T and plots figures
%
% Part 2
% Reads and extracts data from calibration .mat file
% Returns mode, cM, and plots figures.
%
% Usage: [T cM] = plot_vd_beh(subjno,saveflag)


if nargin<2
	saveflag = 0;
end

% Part 1 - visdisc runs
h = figure('Name',subjno);

for t = 1:2
for r = 1:4
	
	% Set filenames
	if t == 1
		dfile = sprintf('%s-1-T-%d.csv',subjno,r); % Target task
	else
		dfile = sprintf('%s-1-D-%d.csv',subjno,r); % Discrimination task
	end
	
	% Extract data
	[T(t).r(r).onset T(t).r(r).face1 T(t).r(r).face2 T(t).r(r).mcond T(t).r(r).resp T(t).r(r).rt] = textread(dfile,'%d%s%s%s%d%d','delimiter',',');
	
end
	
	% Compile data into matrix
	T(t).dat = {vertcat(T(t).r.onset) vertcat(T(t).r.face1) vertcat(T(t).r.face2) vertcat(T(t).r.mcond) vertcat(T(t).r.resp) vertcat(T(t).r.rt)};
		
	% Compute averages and stdev	
	[B I J] = unique(T(t).dat{:,4});
	
	for c = 1:length(B)
		T(t).cond(c).name = B(c);
		T(t).cond(c).resp = T(t).dat{1,5}(strmatch(B(c),T(t).dat{1,4}));
		T(t).cond(c).rt = T(t).dat{1,6}(strmatch(B(c),T(t).dat{1,4}));
		respind = find(T(t).cond(c).resp==4);
		respmid = find(T(t).cond(c).resp==3);
		T(t).cond(c).indprob = length(respind)/length(T(t).cond(c).resp);
		T(t).cond(c).midprob = length(respmid)/length(T(t).cond(c).resp);
		T(t).cond(c).indrt = mean(T(t).cond(c).rt(respind));
		T(t).cond(c).midrt = mean(T(t).cond(c).rt(respmid));
		T(t).cond(c).indrtsd = std(T(t).cond(c).rt(respind));
		T(t).cond(c).midrtsd = std(T(t).cond(c).rt(respmid));
	end
	
	% Plot data
	subplot(3,2,t+t-1,'v6');
	plot(1:length(B),vertcat(T(t).cond.indprob),'r-*'); hold on;
	plot(1:length(B),vertcat(T(t).cond.midprob),'b-*'); hold off;
	if t == 1;
		title('Target task performance');
		legend('Yes','No','Location','Best');
		legend boxoff;
	else
		title('Discrimination task performance');
		legend('Same','Diff','Location','Best');
		legend boxoff;
	end
	set(gca,'XTickLabel',B);
	subplot(3,2,t+t,'v6');
	plot(1:length(B),vertcat(T(t).cond.indrt),'r-*'); hold on;
	plot(1:length(B),vertcat(T(t).cond.midrt),'b-*'); hold off;
	if t == 1;
		title('Target task performance');
		legend('Yes','No','Location','Best');
		legend boxoff;
	else
		title('Discrimination task performance');
		legend('Same','Diff','Location','Best');
		legend boxoff;
	end
	set(gca,'XTickLabel',B);

end


% Part 2 - Calibration phase

cal_file = sprintf('%s-1-C',subjno); % Calibration .mat file
load(cal_file);

cdat = cell2mat(resp(:,3));

subplot(3,2,5,'v6'), plot(1:length(cdat),cdat);
ylim([0 100]);
title(sprintf('Calibration Time Course',cal_file));

cM = mode(cdat);

save T T cM

% Plot calibration graphs
text(10,5,sprintf('Mode = %d',cM));
subplot(3,2,6,'v6'), hist(cdat);
xlim([0 100]);
title(sprintf('Calibration Histogram',cal_file)); 

suptitle(sprintf('Subj No: %s',subjno));
set(h,'Position',[366 230 560 573]);

if saveflag
saveas(h,sprintf('%s_beh.fig',subjno));
end


