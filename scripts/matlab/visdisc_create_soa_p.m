function visdisc_create_soa_p(basedir,filename,run,outputname)

% Set filenames
dfile=sprintf('%s\\%s',basedir,filename);
prtfilename=sprintf('visdisc_run%d_b.prt',run);
firrtcfilename=sprintf('%s_r%d_fir_b.rtc',outputname,run);
hrfrtcfilename=sprintf('%s_r%d_hrf_b.rtc',outputname,run);

% Read behav file data
[ExptName Ss Sess DispRate Grp RandSeed SessDate SessTime Blk Face2ACC Face2DurError Face2OnsDelay Face2OnsTime Face2RT Face2RTTime ITI List1 List1Cycle List1Sample morphcond Pic1 Pic2 Procedure Running]=textread(dfile,'%s%d%d%f%d%d%s%s%d%d%d%d%d%d%d%d%d%d%d%s%s%s%s%s','headerlines',2);

% Config parameters
soa={morphcond, ITI, ITI/2000};
soa{:,3}(1)=16;
for i=2:length(ITI);soa{1,3}(i)=soa{1,3}(i-1)+2+soa{1,2}(i-1)/2000;end

% Extract onsets for each condition
c={'ss' 'sm' 'mm' 'md' 'dd'};
for i=1:5
    cond(i).onsets=soa{1,3}(strmatch(c(i),soa{1,1}));
end

% Print prt for BV use
ccolor(1,:)=[0 0 255];
ccolor(2,:)=[10 100 255];
ccolor(3,:)=[0 255 0];
ccolor(4,:)=[85 170 0];
ccolor(5,:)=[255 170 0];

prtfid = fopen(prtfilename,'w');
fprintf(prtfid,'\n%s\n\n%s\n\n%s\n\n%s\n%s\n%s\n%s\n%s\n%s\n\n%s\n%s\n',...
    sprintf('FileVersion:\t2'),...
    sprintf('ResolutionOfTime:\tVolumes'),...
    sprintf('Experiment:\tvisdisc_r%d',run),...
    sprintf('BackgroundColor:\t255 255 255'),...
    sprintf('TextColor:\t0 0 0'),...
    sprintf('TimeCourseColor:\t0 0 0'),...
    sprintf('TimeCourseThick:\t3'),...
    sprintf('ReferenceFuncColor:\t0 0 80'),...
    sprintf('ReferenceFuncThick:\t3'),...
    sprintf('NrOfConditions:\t5'));

for i=1:length(c)
    fprintf(prtfid,'%s\n%d\n',c{i},length(cond(i).onsets));
    if length(cond(i).onsets>0)
        for j=1:length(cond(i).onsets)
            fprintf(prtfid,'%d\t%d\n',cond(i).onsets(j),cond(i).onsets(j)+1);
        end
    end
    fprintf(prtfid,'Color: %d %d %d\n\n',ccolor(i,1),ccolor(i,2),ccolor(i,3));
end

fclose(prtfid);

% Print fir-rtc for BV use
firrtcfid = fopen(firrtcfilename,'w');
fprintf(firrtcfid,'%s\n%s\n%s\n%s\n\n',...
    sprintf('FileVersion:\t2'),...
    sprintf('Type:\tDesignMatrix'),...
    sprintf('NrOfPredictors:\t45'),...
    sprintf('NrOfDataPoints:\t160'));

for i=1:length(c)
    for j=0:8
        fprintf(firrtcfid,'"%s_D%d" ',c{i},j);
    end
end
fprintf(firrtcfid,'\n');

firmat=zeros(160,45);

for i=1:length(c)
    for j=1:length(cond(i).onsets)
        for k=1:9
            firmat(cond(i).onsets(j)+(k-1),9*(i-1)+k)=1;
        end
    end
end

for i=1:160
    for j=1:45
        fprintf(firrtcfid,'%0.6f ',firmat(i,j));
    end
    fprintf(firrtcfid,'\n');
end

fclose(firrtcfid);

% Print hrf-rtc for BV use
hrf=spm_hrf(2,[6 16 1 1 6 0 18]);
hrfrtcfid = fopen(hrfrtcfilename,'w');
fprintf(hrfrtcfid,'%s\n%s\n%s\n%s\n\n',...
    sprintf('FileVersion:\t2'),...
    sprintf('Type:\tDesignMatrix'),...
    sprintf('NrOfPredictors:\t5'),...
    sprintf('NrOfDataPoints:\t160'));

for i=1:length(c)
    fprintf(hrfrtcfid,'"%s" ',c{i});
end
fprintf(hrfrtcfid,'\n');

firmat=zeros(160,5);

for i=1:length(c)
    firmat(cond(i).onsets,i)=1;
    firmat(:,i)=conv(firmat(:,i),hrf);
end

for i=1:160
    for j=1:length(c)
        fprintf(hrfrtcfid,'%0.6f ',firmat(i,j));
    end
    fprintf(hrfrtcfid,'\n');
end

fclose(hrfrtcfid);
        
% Create soa .mat files for SPM use
soafilename=sprintf('soa_r%d',run);
for i=1:length(c)
    if isempty(cond(i).onsets)
	onsets{i}=0;
	durations(i)={0};
    else
        onsets{i}=cond(i).onsets;
        durations(i)={2};
    end
end
names=c;

save(soafilename,'durations','names','onsets');
