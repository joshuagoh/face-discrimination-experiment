function y = extract_roi_data(roi,P)

% Extracts roi data from P
% MarsBars required.
%
% Usage: y = extract_roi_data(roi,P)
%
% roi - roi structure from MarsBar .mat roi file
% P - filenames of image data to be extracted.

roi_obj = maroi(roi);
y = getdata(roi_obj, P);
