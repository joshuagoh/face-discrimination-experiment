function R = plot_vd_roi_data(nsubj, roidata, roinames)

% Restructures and plots ROI data
% Usage: plot_vd_roi_data(nsubj, roidata, roinames)

% Check arguments
if nargin<2
    saveflag = 0;
end



% Extract data and plot
for r = 1:size(roidata,2)
    k = 1;
    B = roidata(:,r);
    R(r).name = roinames(r);  
    for s = 1:nsubj
        for c = 1:8
            for f = 1:9
                R(r).S(s).C(c).F(f) = B(k);
                k = k+1;
            end            
        end
	
	% Restructure data
	R(r).S(s).T = vertcat(R(r).S(s).C.F);
	
    end
end


% Plot graphs
% Define colors
%ccol = [1 .7 .7; 1 .3 .3; 1 0 0; .7 .7 1; .3 .3 1; 0 0 1; 0 1 0; .3 1 .3];
%
%for s = 1:nsubj
%	h = figure;
%	for r = 1:size(roidata,2)
%		for c = 1:8
%			subplot(size(roidata,2),1,r);
%			plot(1:9, R(r).S(s).C(c).F,'Color',ccol(c,:));
%			hold on;
%		end
%	end
%end
