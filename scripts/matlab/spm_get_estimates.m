% function spm_view_timecourse(varargin)
% View a raw signal timecourse with experimental conditions superimposed
%
% spm_view_timecourse is designed to be called from spm_results_ui.m, but 
% can also be called independently:
%
% - to use as a GUI, call with no parameters
% - from the command line, call with parameters
%	spm_view_timecourse(SPM,xyz,cmd)
%		SPM:	an SPM model structure or filename
%		xyz:	MNI point to evaluate: [xmm ymm zmm] (1x3 array)
%		cmd:	basically a dummy argument.  If it exists, a new
%			TC display figure is opened; if it's not there,
%			the script tries to use an existing figure
%
% - from spm_results_ui.m: 
%	spm_view_timecourse(SPM,hReg,cmd)
%		SPM:	an SPM model structure or filename
%		hReg:	the gui registry containing info about where the
%			pointer is
%		cmd:	basically a dummy argument.  If it exists, a new
%			TC display figure is opened; if it's not there,
%			the script tries to use an existing figure
% 
% ------------------------------------------------------------------------
%
% To use with spm_results_ui.m, add a button to an empty position in the 
% results inference dialog box. When this button is clicked, a timecourse
% of the signal at the current pointer position is plotted
%
% For example: edit spm_results_ui.m, find these lines...
%
% %-Not currently used
% %-----------------------------------------------------------------------
% uicontrol(Finter,'Style','Frame','Position',[010 050 110 030].*WS)
% if strcmp(defaults.modality,'FMRI')
% uicontrol(Finter,'Style','PushButton','String',' ','FontSize',FS(10),...
% 	'ToolTipString',' ',...
% 	'Callback',' ',...
% 	'Interruptible','on','Enable','on',...
% 	'Position',[015 055 100 020].*WS)
% end
%
% ...and replace with:
%
% %-Timecourse viewing
% %-----------------------------------------------------------------------
% uicontrol(Finter,'Style','Frame','Position',[010 050 110 030].*WS)
% if strcmp(defaults.modality,'FMRI')
% uicontrol(Finter,'Style','PushButton','String','Timecourse','FontSize',FS(10),...
% 	'ToolTipString','View the timecourse at current voxel',...
% 	'Callback','spm_view_timecourse(SPM,hReg,''init'');',...
% 	'Interruptible','on','Enable','on',...
% 	'Position',[015 055 100 020].*WS)
% end
%

function Y = spm_get_estimates(varargin)
	
	global svt

	if nargin==0
		SPMfn = spm_select(1,'mat','Select SPM.mat');
		xyz = spm_input;
		eval(sprintf('load %s',SPMfn));
	else
		if ischar(varargin{1})
			eval(sprintf('load %s',varargin{1}));
		else
			SPM = varargin{1};
		end
		
		if length(varargin{2})==3
			xyz = varargin{2};
		else
			hReg = varargin{2};
			xyz = spm_XYZreg('GetCoords',hReg);
		end
	end
	
	% figure
	
	if isfield(svt,'fhdl') && ishandle(svt.fhdl)
		fhdl = svt.fhdl;
		figure(fhdl);
		clf(fhdl);
	else
		if nargin<3, return; end
		scrn = get(0,'Screensize');
		fhdl = figure('Position',[20 50 scrn(3)-40 (scrn(4)/2-100)],...
			'Resize','off',...
			'ToolBar','none',...
			'NumberTitle','off',...
			'MenuBar','none',...
			'PaperPositionMode','auto');
		% zoom handling
		set(fhdl,'WindowButtonDownFcn',{@mousedown});
	end
	
	set(fhdl,'Name',sprintf('Voxel timecourse at %0.0f %0.0f %0.0f',...
				xyz(1),xyz(2),xyz(3)));
	
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	% read contrast estimates
	
	% find voxel indices
	M = SPM.xY.VY(1).mat;
	x = (xyz(1)-M(1,4))/M(1,1);
	y = (xyz(2)-M(2,4))/M(2,2);
	z = (xyz(3)-M(3,4))/M(3,3);

	% sample image volumes !!!!!!!!!!!!!!!!!!!!!!!!!! stopped here
	V = SPM.xY.VY;
	Y = zeros(1,length(V));
	try
		for i=1:length(V)
			Y(i) = spm_sample_vol(V(i),x,y,z,0);
		end
	catch
		l = lasterror;
		fprintf('Could not read data. Path may have changed.\ne.g., %s\n%s\n',...
			V(i).fname,l.message);
		close(svt.fhdl);
		return
	end
	
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	% read & display model onsets
	
	Sess = SPM.Sess;
	
	% find onsets, durations for all conditions
	c = []; ons ={}; dur = {};
	
	TR = SPM.xY.RT;
	for iS = 1:length(Sess)
		sessoffset = sum(SPM.nscan(1:iS-1));
		for ic = 1:length(Sess(iS).U)
			n = Sess(iS).U(ic).name{:};
			i = strmatch(n,c,'exact');
			if isempty(i)
				c = strvcat(c,n);
				i = size(c,1);
			end
			
			onset = sessoffset+(Sess(iS).U(ic).ons'/TR);
			duration = (Sess(iS).U(ic).dur'/TR);
			
			try
				ons{i} = [ons{i} onset];
				dur{i} = [dur{i} duration];
			catch
				ons{i} = onset;
				dur{i} = duration;
			end
		end
	end
	
	
	if isfield(svt,'rangeX')
		rangeX = svt.rangeX;
	else
		rangeX = [1 sum(SPM.nscan)];
	end
	
	% create global structure with info to graph
	svt = struct(...
		'Y',		{Y},...
		'c',		{c},...
		'ons',		{ons},...
		'dur',		{dur},...
		'rangeX',	{rangeX},...
		'nscan',	{SPM.nscan},...
		'xyz',		{xyz},...
		'swd',		{SPM.swd},...
		'fhdl',		fhdl);
	
	wave_plot;
	
function wave_plot
	global svt
	
	% full frame axes
	h = axes('Position',[0 0 1 1],'Visible','off');
	
	rangeX(1) = max([0 floor(svt.rangeX(1))]);
	rangeX(2) = min([sum(svt.nscan)-1 ceil(svt.rangeX(2))]);
		
	rangeY = [min(svt.Y(rangeX(1)+1:rangeX(2)+1)) ...
		  max(svt.Y(rangeX(1)+1:rangeX(2)+1))];
	
	% graph axes
	waveaxes = axes('Position',[.05 .15 .9 .8]);
	hold on;
	
	% define colors
	cinterval = round(64/length(svt.ons));
	cmap = 1:cinterval:1+cinterval*length(svt.ons);
	barheight = ceil(rangeY(2))-floor(rangeY(1));
	yy = floor(rangeY(1)):ceil(rangeY(2));
	
	for ic=1:length(svt.ons)
		thisons = svt.ons{ic};
		thisdur = max(1,svt.dur{ic});
		for ions=1:length(thisons)
			C = cmap(ic)*ones(barheight,ceil(thisdur(ions)));
			xx = thisons(ions):thisons(ions)+thisdur(ions)-1;
			imhdl = image(.5+xx,yy,C);
		end
	end
	
	axis([rangeX(1) rangeX(2) ...
		floor(rangeY(1)) ceil(rangeY(2))]);	
	
	
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	% display timecourse waveform
			
	wave = plot(0:length(svt.Y)-1,svt.Y,'k');
	set(wave,'LineWidth',2);
	
	% draw session dividers
	divxx = cumsum(svt.nscan);
	for i=divxx(1:length(divxx)-1)
		line(i*ones(1,2),[floor(rangeY(1)) ceil(rangeY(2))],...
			'LineStyle','--',...
			'LineWidth',3,...
			'Color',[0 0 0]);
	end
	
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	% display labels
	%
	
	set(gcf,'CurrentAxes',h)
	axis([0 100 0 100]);
	hold on;
	
	% draw legend
	m = colormap;
	legendspacing = 90/length(svt.ons);
	legendwidth = 3; legendheight = 5;
	yy = 3; 
	
	rectangle('Position',[3 yy-1 93 legendheight+2]);
	for i=1:length(svt.ons)
		xx = 5+legendspacing*(i-1);
		rectangle('Position',[xx yy legendwidth legendheight],...
			'FaceColor',m(cmap(i),:));
		text(xx+legendwidth+1,yy+1,fixu(svt.c(i,:)));
	end
	
	% save figure button
	fpos = get(gcf,'Position');
	uicontrol(gcf,'Style','PushButton','String','Save','FontSize',9,...
		'ToolTipString','Save the graph',...
		'ButtonDownFcn',{@savettc},...
		'Interruptible','on','Enable','inactive',...
		'Position',[fpos(3)-55 fpos(4)-25 50 020])

%% event handlers
		
function mousedown(h,e)
	global svt
	pos = get(gca,'currentpoint');
	svt.rangeX(1) = pos(1);
	set(h,'WindowButtonUpFcn',{@mouseup});

		
function mouseup(h,e)
	global svt
	pos = get(gca,'currentpoint');
	svt.rangeX(2) = pos(1);
	% if you're moving right to left, zoom out to original size
	if svt.rangeX(2)<=svt.rangeX(1)
		svt.rangeX = [1 sum(svt.nscan)];
	end
	% redraw
	clf(h);
	wave_plot;

function savettc(h,e)
	global svt
	xyz = svt.xyz;
	swd = svt.swd;
	set(h,'Visible','off');
	saveas(gcf,fullfile(swd,...
		sprintf('ttc_%d_%d_%d.png',xyz(1),xyz(2),xyz(3))));
	set(h,'Visible','on');
	
function fixed = fixu(in)
	fixed = strrep(in,'_',' ');
