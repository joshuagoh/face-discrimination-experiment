function C = fdaa_extract_topvox_VV_VOI(peakcoord,vmp,width,nvox)

% Set params
dimx = size(vmp.Map(1).VMPData,1);
dimy = size(vmp.Map(1).VMPData,2);
dimz = size(vmp.Map(1).VMPData,3);
Y    = zeros(dimx,dimy,dimz);

% Convert TAL peak to BV sys
[x,y,z] = Tal2BVsys(peakcoord(1),peakcoord(2),peakcoord(3));

% Create ROI search box based on width and peak coord
xs = round(x - width/2); xe = round(x + width/2);
ys = round(y - width/2); ye = round(y + width/2);
zs = round(z - width/2); ze = round(z + width/2);

% Extract top N voxels from box
Y(xs:xe,ys:ye,zs:ze) = vmp.Map(1).VMPData(xs:xe,ys:ye,zs:ze);

I    = [Y(find(Y)) find(Y)];
I    = sortrows(I,-1);
Itop = I(1:nvox,2);

[ix,iy,iz] = ind2sub(size(Y),Itop);

C = [ix iy iz];