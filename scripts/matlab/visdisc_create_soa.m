function visdisc_create_soa(basedir,filename,run)

% Set filenames
dfile=sprintf('%s/%s',basedir,filename);
prtfilename=sprintf('visdisc_run%d_b.prt',run);
firrtcfilename=sprintf('pilotvd1092807_r%d_fir_b.rtc',run);
hrfrtcfilename=sprintf('pilotvd1092807_r%d_hrf_b.rtc',run);

% Read behav file data
[ExptName Ss Sess DispRate Grp RandSeed SessDate SessTime Blk Face2ACC Face2DurError Face2OnsDelay Face2OnsTime Face2RESP Face2RT Face2RTTime ITI List1 List1Cycle List1Sample morphcond Pic1 Pic2 Procedure Running]=textread(dfile,'%s%d%d%f%d%d%s%s%d%d%d%d%d%d%d%d%d%d%d%d%s%s%s%s%s','headerlines',2);

% Exception in Atsunobu's case
morphcond(strmatch('md',morphcond))={'mm'};

% Config parameters
soa={[cell2mat(morphcond) num2str(Face2RESP)], ITI, ones(36,1)};
soa{:,3}(1)=15;
for i=2:36;soa{1,3}(i)=soa{1,3}(i-1)+2+(soa{1,2}(i-1)/2000);end

% Extract onsets based on response: '4' - SAME, '3' - DIFFERENT
c={'ss4' 'sm4' 'mm4' 'mm3' 'dd3' 'dummy'};
for i=1:5
    cond(i).onsets=soa{1,3}(strmatch(c(i),soa{1,1}));
end

c2={'ss3' 'sm3' 'dd4'};d=[];
for i=1:3
    d=vertcat(d,strmatch(c2(i),soa{1,1}));
end

cond(6).onsets=soa{1,3}(vertcat(d,find(Face2RESP==0)));

% Print prt
ccolor(1,:)=[0 0 255];
%ccolor(2,:)=[10 100 255];
ccolor(2,:)=[85 170 255];
%ccolor(4,:)=[95 200 255];
ccolor(3,:)=[0 255 0];
ccolor(4,:)=[85 170 0];
%ccolor(7,:)=[255 0 0];
ccolor(5,:)=[255 170 0];
ccolor(6,:)=[100 100 100];

prtfid = fopen(prtfilename,'w');
fprintf(prtfid,'\n%s\n\n%s\n\n%s\n\n%s\n%s\n%s\n%s\n%s\n%s\n\n%s\n%s\n',...
    sprintf('FileVersion:\t2'),...
    sprintf('ResolutionOfTime:\tVolumes'),...
    sprintf('Experiment:\tvisdisc_r%d',run),...
    sprintf('BackgroundColor:\t255 255 255'),...
    sprintf('TextColor:\t0 0 0'),...
    sprintf('TimeCourseColor:\t0 0 0'),...
    sprintf('TimeCourseThick:\t3'),...
    sprintf('ReferenceFuncColor:\t0 0 80'),...
    sprintf('ReferenceFuncThick:\t3'),...
    sprintf('NrOfConditions:\t6'));

for i=1:6
    fprintf(prtfid,'%s\n%d\n',c{i},length(cond(i).onsets));
    if length(cond(i).onsets>0)
        for j=1:length(cond(i).onsets)
            fprintf(prtfid,'%d\t%d\n',cond(i).onsets(j),cond(i).onsets(j)+1);
        end
    end
    fprintf(prtfid,'Color: %d %d %d\n\n',ccolor(i,1),ccolor(i,2),ccolor(i,3));
end

fclose(prtfid);

% Print fir-rtc
firrtcfid = fopen(firrtcfilename,'w');
fprintf(firrtcfid,'%s\n%s\n%s\n%s\n\n',...
    sprintf('FileVersion:\t2'),...
    sprintf('Type:\tDesignMatrix'),...
    sprintf('NrOfPredictors:\t54'),...
    sprintf('NrOfDataPoints:\t186'));

for i=1:6
    for j=0:8
        fprintf(firrtcfid,'"%s_D%d" ',c{i},j);
    end
end
fprintf(firrtcfid,'\n');

firmat=zeros(186,54);

for i=1:6
    for j=1:length(cond(i).onsets)
        for k=1:9
            firmat(cond(i).onsets(j)+(k-1),9*(i-1)+k)=1;
        end
    end
end

for i=1:186
    for j=1:54
        fprintf(firrtcfid,'%0.6f ',firmat(i,j));
    end
    fprintf(firrtcfid,'\n');
end

fclose(firrtcfid);

% Print hrf-rtc
hrf=spm_hrf(2,[6 16 1 1 6 0 18]);
hrfrtcfid = fopen(hrfrtcfilename,'w');
fprintf(hrfrtcfid,'%s\n%s\n%s\n%s\n\n',...
    sprintf('FileVersion:\t2'),...
    sprintf('Type:\tDesignMatrix'),...
    sprintf('NrOfPredictors:\t6'),...
    sprintf('NrOfDataPoints:\t186'));

for i=1:6
    fprintf(hrfrtcfid,'"%s" ',c{i});
end
fprintf(hrfrtcfid,'\n');

firmat=zeros(186,6);

for i=1:6
    firmat(cond(i).onsets,i)=1;
    firmat(:,i)=convn(firmat(:,i),hrf,'same');
end

for i=1:186
    for j=1:6
        fprintf(hrfrtcfid,'%0.6f ',firmat(i,j));
    end
    fprintf(hrfrtcfid,'\n');
end

fclose(hrfrtcfid);
        
