% Generic script to create stimulus onset files from behavioral output files.
% Modified from create_soa_d.m, 2 Aug 2009
% Usage: create_soa_d_resp(idir,behfname,odir,exptname,subjno,task,run)

function create_soa_d_resp(idir,behfname,odir,exptname,subjno,task,run)

% Set filenames
dfile=sprintf('%s/%s',idir,behfname);
prtfilename=sprintf('%s/%s_%s_%s_r%d_d_resp.prt',odir,exptname,subjno,task,run);
firrtcfilename=sprintf('%s/%s_%s_%s_r%d_fir_d_resp.rtc',odir,exptname,subjno,task,run);
hrfrtcfilename=sprintf('%s/%s_%s_%s_r%d_hrf_d_resp.rtc',odir,exptname,subjno,task,run);
hrfparrtcfilename=sprintf('%s/%s_%s_%s_r%d_parhrf_d_resp.rtc',odir,exptname,subjno,task,run);
soafilename=sprintf('%s/%s_%s_%s_r%d_soa_d_resp',odir,exptname,subjno,task,run);

% Read behav file data
[onset face1 face2 mcond resp rt]=textread(dfile,'%d%s%s%s%d%d','delimiter',',');
clear rt;

% Config parameters
nscans = 109-3; firorder = 9;
for i=1:length(mcond)
    if isempty(resp(i)) || (strcmp(mcond{i},'md1') && resp(i)==3) || (strcmp(mcond{i},'md3') && resp(i)==4)
            mcond{i} = ['t' task];
    else
        mcond{i} = [mcond{i} task num2str(resp(i))];
    end
end
soa={mcond, (round(onset/2000)-3)}; % offset 3 initial scans
hrf=spm_hrf(2,[6 16 1 1 6 0 18]);
c={'md1d4' 'md2d4' 'md2d3' 'md3d3' 'td'}; % Condition names
dur = 1; % Stimulus durations (scans)

% Extract onsets for each condition
npred = length(c);
for i=1:length(c)
    cond(i).onsets=soa{1,2}(strmatch(c(i),soa{1,1}));
end

% Print prt for BV use
ccolor(1,:)=[170 170 255];
ccolor(2,:)=[110 110 255];
ccolor(3,:)=[60 60 255];
ccolor(4,:)=[0 0 255];
ccolor(5,:)=[0 255 0];

prtfid = fopen(prtfilename,'w');
fprintf(prtfid,'\n%s\n\n%s\n\n%s\n\n%s\n%s\n%s\n%s\n%s\n%s\n\n%s\n%s\n',...
    sprintf('FileVersion:\t2'),...
    sprintf('ResolutionOfTime:\tVolumes'),...
    sprintf('Experiment:\t%s_%s_%s_r%d',exptname,subjno,task,run),...
    sprintf('BackgroundColor:\t255 255 255'),...
    sprintf('TextColor:\t0 0 0'),...
    sprintf('TimeCourseColor:\t0 0 0'),...
    sprintf('TimeCourseThick:\t3'),...
    sprintf('ReferenceFuncColor:\t0 0 80'),...
    sprintf('ReferenceFuncThick:\t3'),...
    sprintf('NrOfConditions:\t%d', length(c)));

for i=1:npred
    fprintf(prtfid,'%s\n%d\n',c{i},length(cond(i).onsets));
    if length(cond(i).onsets>0)
        for j=1:length(cond(i).onsets)
            fprintf(prtfid,'%d\t%d\n',cond(i).onsets(j),cond(i).onsets(j)+1);
        end
    end
    fprintf(prtfid,'Color: %d %d %d\n\n',ccolor(i,1),ccolor(i,2),ccolor(i,3));
end

fclose(prtfid);

% Print fir-rtc for BV use
firrtcfid = fopen(firrtcfilename,'w');
fprintf(firrtcfid,'%s\n%s\n%s\n%s\n\n',...
    sprintf('FileVersion:\t2'),...
    sprintf('Type:\tDesignMatrix'),...
    sprintf('NrOfPredictors:\t%d',npred*firorder),...
    sprintf('NrOfDataPoints:\t%d',nscans));

for i=1:npred
    for j=0:(firorder-1)
        fprintf(firrtcfid,'"%s_D%d" ',c{i},j);
    end
end
fprintf(firrtcfid,'\n');

firmat=zeros(nscans,firorder*npred);

for i=1:npred
    for j=1:length(cond(i).onsets)
        for k=1:firorder
            firmat(cond(i).onsets(j)+(k-1),firorder*(i-1)+k)=1;
        end
    end
end

for i=1:nscans
    for j=1:(firorder*npred)
        fprintf(firrtcfid,'%0.6f ',firmat(i,j));
    end
    fprintf(firrtcfid,'\n');
end

fclose(firrtcfid);

% Print hrf-rtc for BV use
hrfrtcfid = fopen(hrfrtcfilename,'w');
fprintf(hrfrtcfid,'%s\n%s\n%s\n%s\n\n',...
    sprintf('FileVersion:\t2'),...
    sprintf('Type:\tDesignMatrix'),...
    sprintf('NrOfPredictors:\t%d',npred),...
    sprintf('NrOfDataPoints:\t%d',nscans));

for i=1:npred
    fprintf(hrfrtcfid,'"%s" ',c{i});
end
fprintf(hrfrtcfid,'\n');

firmat=zeros(nscans,npred);

for i=1:npred
    firmat(cond(i).onsets,i)=1;
    hrfmat(:,i)=conv(firmat(:,i),hrf);
end

for i=1:nscans
    for j=1:npred
        fprintf(hrfrtcfid,'%0.6f ',hrfmat(i,j));
    end
    fprintf(hrfrtcfid,'\n');
end

fclose(hrfrtcfid);

% Print parametric hrf_rtc for BV use
hrfparrtcfid = fopen(hrfparrtcfilename,'w');
fprintf(hrfparrtcfid,'%s\n%s\n%s\n%s\n\n',...
    sprintf('FileVersion:\t2'),...
    sprintf('Type:\tDesignMatrix'),...
    sprintf('NrOfPredictors:\t%d',npred),...
    sprintf('NrOfDataPoints:\t%d',nscans));

for i=1:npred
    fprintf(hrfparrtcfid,'"%s" ',c{i});
end
fprintf(hrfparrtcfid,'\n');

for i=1:npred
    hrfmat(:,i)=hrfmat(:,i)*i/npred;
end

for i=1:nscans
    for j=1:npred
        fprintf(hrfparrtcfid,'%0.6f ',hrfmat(i,j));
    end
    fprintf(hrfparrtcfid,'\n');
end

fclose(hrfparrtcfid);

% Create soa .mat files for SPM use
for i=1:npred
    onsets{i}=cond(i).onsets;
    durations(i)={dur};
end
names=c(1:npred);


save(soafilename,'durations','names','onsets');
