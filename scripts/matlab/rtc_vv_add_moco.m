% Add motion correction parameters to VV rtc files. 

function rtc_vv_add_moco(bdir,ortc,mrtc)

ortcfname = sprintf('%s/%s.rtc',bdir,ortc);
mrtcfname = sprintf('%s/%s.rtc',bdir,mrtc);

ofname = sprintf('%s/%s_3DMC.rtc',bdir,ortc);

ofid = fopen(ofname,'wt');

% Read MOCO data
[xt yt zt xr yr zr] = textread(mrtcfname,'%f%f%f%f%f%f','headerlines',6);

% Read VV data
[f p s] = textread(ortcfname,'%f%f%f','headerlines',6);

% Combine MOCO and VV data
data = [f p s xt yt zt xr yr zr];

% Write 3DMC rtc headerlines
fprintf(ofid,'%s\t%s\n%s\t%s\n%s\t%s\n%s\t%s\n\n%s\n',...
    'FileVersion:','2',...
    'Type:','DesignMatrix',...
    'NrOfPredictors:','9',...
    'NrOfDataPoints:','185',...
    '"face" "place" "scrambled" "xtrans" "ytrans" "ztrans" "xrot" "yrot" "zrot"');

% Write 3DMC rtc data
for i = 1:size(data,1)
    for j = 1:size(data,2) 
       fprintf(ofid,'%f ',data(i,j));
    end
    fprintf(ofid,'\n');
end

fclose(ofid);