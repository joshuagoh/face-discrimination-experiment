function B = fdaa_convert_BVBetaTable2CSV(bdir,BVBetaTableTxt,mocoflag,VVflag)

dfname = sprintf('%s/%s.txt',bdir,BVBetaTableTxt);

S     = importdata(dfname);
D     = S.data;
nroi  = size(D,2);
nsubj = 40;

if mocoflag==0
    nmoco = 0;
else
    nmoco = 6;
end

if VVflag==0
    ncond = 4; % 4 for no Resp, 5 for with Resp
    nfirt = 9;
else
    ncond = 3;
    nfirt = 1;
end
npred = ncond*nfirt;

ofid  = fopen(dfname);

for r = 1:nroi

    B(r).B = zeros(nsubj,npred);
    rname  = fscanf(ofid,'%s',1);
    rname  = regexprep(rname,'"','');
    ofname = sprintf('%s/%s_%s.csv',bdir,BVBetaTableTxt,rname);

    k = 1;

    for i = 1:nsubj
        B(r).B(i,:) = D(k:k+npred-1,r);
        k           = k+npred+nmoco;
    end

    csvwrite(ofname,B(r).B);

end

fclose(ofid);