function B = fdaa_extract_topNvoxel_beta_script(VOIname,PC,subjno,glmfname)

glm  = BVQXfile(glmfname);
voxn = [10 15 20];

for s = 1:size(subjno,1)
    peakcoord = PC(s,:);
    vmpfname  = sprintf('/net/data9/visdisc/bv/VDR%s/VDR%s_f-h_Tmap.vmp',subjno(s,:),subjno(s,:));
    vmp       = BVQXfile(vmpfname); % T-map from Face > House contrast

    for v = 1:length(voxn)
        C = fdaa_extract_topvox_VV_VOI(peakcoord,vmp,20,voxn(v));        
        glmB = glm.GLMData.Subject(s).BetaMaps;
        B.nvox(v).B(s,:) = fdaa_extract_voxel_beta(C,glmB);
    end
end

% Write csv
for v = 1:length(voxn)
    csvfname = sprintf('/net/data9/visdisc/bv/N40VV/3DMC/N40VV_f-h_Indiv_%s_top%dvox_Disc_Betas.csv',VOIname,voxn(v));
    csvwrite(csvfname);
end