function create_roi_sphere(x,y,z,name,radius)

% Creates sphere roi from voxel coordinates and radius.
% MarsBar required.
% Saves .mat roi in current directory.
%
% Usage: create_roi_sphere(x,y,z,name,radius)
%
% name - string of format 'contrast_roiname'
% radius - in mm

p.centre=[x y z];
p.label=sprintf('%s_sphere_%d-%d_%d_%d',name,radius,x,y,z);
p.descrip=sprintf('%d.0mm radius sphere at %s',mat2str(p.centre));
p.radius=radius;
croi=maroi_sphere(p);
saveroi(croi,sprintf('%s_roi.mat',p.label));
