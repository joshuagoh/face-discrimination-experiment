function bwframe(pair1,pair2,frame)

bdir=pwd;
folders=dir;

binF=double(imread(frame))/255;

for i=pair1:pair2
    for j=0:99
        Iname=sprintf('%s/%s/%s_%d.bmp',bdir,folders(i).name,folders(i).name,j);
        I=double(imread(Iname));I=rgb2gray(uint8(I.*binF));
        imwrite(I,Iname);
    end
end
