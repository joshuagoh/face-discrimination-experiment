function visdisc_addmoco2rtc(subjno)

for i = 1:size(subjno,1)
    bdir = sprintf('/net/data9/visdisc/bv/VDR%s',subjno(i,:));
    
    % VV
    ortc = sprintf('vv_%s_hrf',subjno(i,:));
    mrtc = sprintf('VDR%s_vv_SCLAI2_3DMC',subjno(i,:));
    
    rtc_vv_add_moco(bdir,ortc,mrtc);
    
    % Disc
    for r = 1:4
        ortc = sprintf('VD_%s_d_r%d_fir_d',subjno(i,:),r);
        mrtc = sprintf('VDR%s_td_r%d_SCLAI2_3DMC',subjno(i,:),r);
        
        rtc_td_add_moco(bdir,ortc,mrtc);
    end
    
    % Targ
    for r = 1:4
        ortc = sprintf('VD_%s_t_r%d_fir_t',subjno(i,:),r);
        mrtc = sprintf('VDR%s_tt_r%d_SCLAI2_3DMC',subjno(i,:),r);
        
        rtc_td_add_moco(bdir,ortc,mrtc);
    end
end