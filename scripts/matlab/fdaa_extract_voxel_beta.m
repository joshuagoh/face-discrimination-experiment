function B = fdaa_extract_voxel_beta(C,glmB)

[x y z k] = size(glmB);

I = sub2ind(size(glmB),C(:,1),C(:,2),C(:,3));
B = zeros(k);

for pred = 1:k
    D = glmB(:,:,:,k);
    B(pred) = mean(D(I));
end
