% Add motion correction parameters to TD rtc files. 

function rtc_td_add_moco(bdir,ortc,mrtc)

ortcfname = sprintf('%s/%s.rtc',bdir,ortc);
mrtcfname = sprintf('%s/%s.rtc',bdir,mrtc);

ofname = sprintf('%s/%s_3DMC.rtc',bdir,ortc);

ofid = fopen(ofname,'wt');

% Read MOCO data
[xt yt zt xr yr zr] = textread(mrtcfname,'%f%f%f%f%f%f','headerlines',6);

% Read TD data
[data] = textread(ortcfname,'','headerlines',6);

% Combine MOCO and VV data
data = [data(:,1:36) xt yt zt xr yr zr];

% Write 3DMC rtc headerlines
fprintf(ofid,'%s\t%s\n%s\t%s\n%s\t%s\n%s\t%s\n\n',...
    'FileVersion:','2',...
    'Type:','DesignMatrix',...
    'NrOfPredictors:','42',...
    'NrOfDataPoints:','106');

C={'md1','md2','md3','t'};
task = ortc(18);

for i = 1:4
    for j = 0:8
        fprintf(ofid,'"%s%s_D%d" ',C{i},task,j);
    end
end

fprintf(ofid,'%s\n','"xtrans" "ytrans" "ztrans" "xrot" "yrot" "zrot"');   

% Write 3DMC rtc data
for i = 1:size(data,1)
    for j = 1:size(data,2) 
       fprintf(ofid,'%f ',data(i,j));
    end
    fprintf(ofid,'\n');
end

fclose(ofid);