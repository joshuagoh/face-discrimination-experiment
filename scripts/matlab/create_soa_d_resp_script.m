function create_soa_d_resp_script(subjno)

iidir = '/net/data9/visdisc/behav_output/visdisc/actual';
oodir = iidir;

for i = 1:size(subjno,1)
    idir = sprintf('%s/%s',iidir,subjno(i,:));
    odir = idir;
    exptname = 'VD';
    task = 'd';
    for r = 1:4
        behfname = sprintf('%s-1-D-%d.csv',subjno(i,:),r);
        create_soa_d_resp(idir,behfname,odir,exptname,subjno(i,:),task,r);
    end
end
        