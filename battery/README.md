# List of Neuropsychological Tests

1. Pattern Matching
2. Digit Symbol
3. Dot Matching
4. Spatial Span
5. Digit Span
6. Word Lists I
7. Mini Mental State Exam
8. Wechsler Memory Scale Face Memory
9. Benton Facial Recognition Test
